program mesh
! use initializeNRK
  implicit none
  
  double precision :: xa = 0.d0 ! Position of first boundary
  double precision :: xb = 50.d0 ! Postition of second boundary in equally spaced mesh
  double precision :: x_limit = 50.d0 ! Postition of second limit in modified spaced mesh
  double precision :: rhod_sum, x_step

!   double precision, dimension(500) :: x
  double precision, dimension(:), allocatable :: x, rho, rhod, x_new, w, rhodd, aa
  integer, dimension(:), allocatable :: wn
  integer :: i, j, nn, k, w_sum, t


  nn = 600; rhod_sum = 0.d0; w_sum = 0
  k = 2
  x_step = xb/nn
!   write(*,*) '- Starting mesh -'
  open (unit=91, file='rho_equally_spaced.dat')
  open (unit=90, file='rho_new_mesh.dat')
  open (unit=92, file='mesh.input')
  allocate(x(nn)); allocate(rho(nn)); allocate(rhod(nn)); allocate(w(nn)); allocate(wn(nn))
  allocate(rhodd(nn)); allocate(aa(nn))
  do i = 1, nn
     x(i) = xa+(xb-xa)*(i-1.d0)/(nn-1.d0)
     rho(i) = 1.d0/((1.d0 + x(i)*x(i)/8.d0)**2.d0)
     rhod(i) = dabs(-2.d0 * (x(i)/4.d0) / ((1.d0 + x(i)*x(i)/8.d0)**3.d0))
     rhodd = dabs(3.d0/8.d0*x*x / ((1.d0 + x*x/8.d0)**4.d0) - 0.5d0/((1.d0 + x*x/8.d0)**3.d0))
     aa(i) = dsqrt( (rho(i)*rhodd(i)+rhod(i)**2.d0) / rhodd(i)**2.d0)
     rhod_sum = rhod(i) + rhod_sum
!      write(91,*) x(i), rho(i), dabs(rhod(i))
  enddo
  
  do i = 1, nn
     wn(i) = nint(rhod(i)/rhod_sum*nn)
     w(i) = (rhod(i)/rhod_sum*nn)
     w_sum = wn(i) + w_sum
     write(91,*) x(i), rho(i), rhod(i), rhodd(i), aa(i), w(i), wn(i)
  enddo
  print*,rhod_sum, w_sum
  
  allocate(x_new(nn*w_sum))
  x_new = 0.d0
  do i = 1, nn
     if (wn(i) == 0) wn(i) = 1
     do j = 1, wn(i)
!         print*, k, x_step/wn(i)*j
        if (w(i) == 0) then 
           x_new(k) = x(i) + x_step
        else
           x_new(k) = x(i) + x_step/w(i)*j
        endif
        k = k + 1
     enddo
  enddo
  print*,'k=', k
  
  deallocate(rho); allocate(rho(nn*w_sum))
  do i = 1, k -1
     rho(i) = 1.d0/((1.d0 + x_new(i)*x_new(i)/8.d0)**2.d0)
     if (x_new(i) <= x_limit) then
        write(90,*) i, x_new(i), x_new(i+1), rho(i), x_new(i+1) - x_new(i)
        write(92,*) x_new(i)
        t = t + 1
     endif
  enddo
  print*,'Number of meshpoints is', t
end program mesh